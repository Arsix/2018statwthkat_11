                                                        ТЕМА: СТАТЬИ С КАТЕГОРИЯМИ
                                                           ТЕХНИЧЕСКОЕ ЗАДАНИЕ


ЦЕЛЬ ПРОЕКТА

 Целью данного проекта является разработка электронной статьи с категориями с последующим размещением в сети Интернет. 
Проект должен обеспечивать реализацию следующей функции:
     1)	Информационная.
Статья должна предоставлять доступ к информации о коде, авторе, тексту, дате добавления и коде раздела. Для каждого раздела - его код, название и текстовое описание.

1. ЭКСПЛУАТАЦИОННОЕ НАЗНАЧЕНИЕ
 Областью применения данного проекта может являться онлайн собрание научных статей. От проекта ожидается – обеспечить возможность добавления и удаления как статей, так и разделов, просмотра статей по разделам, просмотра конкретной статьи и списка разделов с целью повышения интеллектуального уровня и расширения кругозора пользователей и участников данного ресурса, а так же с возможностью оставлять комментарии. Предполагается реализация авторизации пользователя.

2. Функциональное назначение
 Техническим средством, реализующим выгоду, является ПК. Будет установлено всё необходимое ПО. Будет создана БД и наполнена данными которые будут содержать в себе статьи разделенные по категориям для которых будут сделаны, так же будет реализовано разграничение доступа с помощью MySQL. По средствам языков программирования SQL и PHP будут реализованы запросы на выборку, удаление, добавление статей и разделов и возможность оставлять комментарии.

3. Термины и определения
Статья -  Глава, раздел в каком-нибудь документе, перечне, справочнике.
ПК - персональный компьютер;
ПО - программное обеспечение;
БД- база данных;
Категория - определении наиболее фундаментальных и широких классов сущностей.

4. Типы данных
Статья:
  Код – числовое;
  Автор – текстовое;
  Название - текстовое;
  Текст – текстовое;
  Дата добавления – дата;
  Код раздела – числовое;
  Код пользователя – числовой;
Раздел:
  Код – числовое;
  Название – текстовое;
  Текстовое описание – текстовое;
Пользователь:
  Код - числовое;
  Логин - текстовое;
  Пароль - текстовое;
  Адрес электронной почты - текстовое;
  Статус – логическое;
Комментарии:
  Код - числовое;
  Автор - текстовое;
  Дата - дата;
  Текст - текстовое;
5. Функциональные характеристики
 Возможность добавления и удаления как страниц, так и разделов, просмотра статей по разделам, просмотра конкретной статьи и списка разделов.
Зарегистрированный пользователь может составить статью после составления статьи она поступает на премодерацию для проверки статьи на достоверность информации, далее если статья проходит проверку, тогда её распределяют по категории и уже после этого её публикуют иначе публикацию отклоняют по какой - либо причине. Неавторизованный пользователь имеет доступ только для чтения статей.
<img src="Jpg/1.bmp">
6. Страницы
<img src="Jpg/2.bmp">
<img src="Jpg/3.bmp">
7. Требования к надежности
 Максимальная нагрузка на сайт – до 1000 одновременных запросов на сайте. Для этого будет использоваться современное оборудование.
8. Требования к хостингу
Apache 2.2.22  
mod_ssl 2.2.22 
OpenSSL 1.0.1c 
PHP5 3.13
9. Наполнение контентом
 Начальное наполненность контента может составлять до 10000 статей с возможностью увеличения хранимой информации.
10. Сдача и приемка
 Сдача проекта – 25 мая 2018.

